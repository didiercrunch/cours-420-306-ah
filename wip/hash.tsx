import ReactNiceAvatar, {genConfig} from 'https://esm.sh/react-nice-avatar@1.4.1';
// @deno-types="https://denopkg.com/soremwar/deno_types/react/v16.13.1/react.d.ts"
import React from "https://jspm.dev/react@17.0.2";

// @deno-types="https://denopkg.com/soremwar/deno_types/react-dom/v16.13.1/server.d.ts"
import ReactDOMServer from "https://jspm.dev/react-dom@17.0.2/server";


function onlyKeepSvg(text: string): string {
    const startIndex = text.indexOf("<svg");
    text = text.substring(startIndex);
    const endIndex = text.lastIndexOf("</svg>") + "</svg>".length;
    return text.substring(0, endIndex);
}

const handler = (request: Request): Response => {
    console.log(new URL(request.url).pathname)
    const config = genConfig(new URL(request.url).pathname);
    const body = ReactDOMServer.renderToStaticMarkup(
        <ReactNiceAvatar {...config}/>
    );
    const mimetype = "text/html"; //
    return new Response(body, {
        status: 200,
        headers: {
            "content-type": mimetype,
        }
    });
};


const port = 8080;
console.log(`HTTP server running. Access it at: http://localhost:${port}/`);
Deno.serve({port}, handler);