#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Robot Wanted League", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1)
}
#set heading(numbering: numbering-function, level: 1)


= Objectif

Votre mission, si vous l'accepter, est de créer un répertoire web de tous les 
vilains robots recherchés par la _Robot Wanted League™_.  Le président veut
ce site web au plus coupant et se garde le droit de mondifier ce qu'il veut
quand bon lui semble.


== Produit Minimal Viable

Il faut commencer par créer une page qui listera tous les robots recherchés
ainsi que, pour chaque robot recherché, une page avec les informations 
suivantes.

- Le nom du robot
- Le poid du robot
- La taille du robot
- Le pays ou le robot se trouve


Nous avons déjà trois robots à lister:

1. Alice, pèse 6 578kg pour 17.76m de haut.  La dernière fois qu'il a été vu il était en Albanie
2. Bob, pèse 10 871kg pour 15m de haut.  La dernière fois qu'il a été vu il était au Bénin
3. Cédric, pèse 1 002kg pour 3.12m de haut.  La dernière fois qu'il a été vu il était en Cambodge


== Ajouter Des Robots

Finalement, les agents sur le terrain veulent être capable d'ajouter des robots
à l'application.  Les agents doivent être capable de specifier un nom de robot,
un poid, une taille et un pays pour créer un nouveau robot.


== Les Robots Ont La Bougeotte

Des agents nous ont dit que certain robots commence à changer de pays!  Vite, vite,
il faut continuer de les suivre à la trace.  Il faut absolument que les agents puissent
changer le lieu où se trouve un robot.

=== 
Je ne sais pas si c'est techniquement possible, mais il faudrait que ce soit possible
savoir où les robots on été dans le passé.  Ce serait stupide de perdre l'information
d'où était les robots.


== Trop d'Erreur

Beaucoup de nos agents proviennent du Cegep-Du-Vieux-Montréal et sont très limité
côté géographie.  Un agent à dit avoir vue le Robot Garry à Aghraba!  Aghraba c'est
le pays d'Aladin, ça n'existe pas!
