#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Les Bases Du C#", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1)
}
#set heading(numbering: numbering-function, level: 1)



#let link-palindome = link("https://fr.wikipedia.org/wiki/Palindrome")[palindrome]
#let link-carre-parfait = link("https://fr.wikipedia.org/wiki/Carr%C3%A9_parfait")[carré parfait]
#let link-fibb = link("https://en.wikipedia.org/wiki/Fibonacci_sequence")[nombres de Fibonacci]
#let link-us-timeformat = link("https://fr.wikipedia.org/wiki/Syst%C3%A8me_horaire_sur_12_heures")[heure de format américaine]
#let link-typst = link("https://typst.app")[typst]


== À Propos de $n$

Écrire un programme qui prend un nombre $n$ et qui..

+ Imprime "impair" si le nombre est impair
+ Imprime "petit nombre pair" si le nombre est pair et inclus entre 0 et 20.
+ Imprime "grand nombre pair" si le nombre est pair et inclus entre 20 et 100.
+ Imprime "nombre pair et negatif" si le nombre est pair et strictement négatif.


== Carré Parfait
Écrire un programme qui verifie si un nombre est un #link-carre-parfait.

source: https://leetcode.com/problems/valid-perfect-square/


== Multiple de 3 et 5

Écrire un programme qui imprime les 10 premiers multiples de 5 qui sont aussi 
des multiples de 3 (15, 30, 45, ...).


== Nombre Laid
Un nombre laid est un nombre uniquement divisible par 2, 3 ou 5.  Écrire
un programme qui identifie si un nombre est laid.

source: https://leetcode.com/problems/ugly-number/



== Transformateur d'Heure

Écrire un programme qui transforme une #link-us-timeformat à une heure de format internationale. 
Par example, `01:15 PM` #sym.arrow.r `13:15`. 


== Convertisseur de Température
Écrire un programme qui convertie un température de Fahrenheit en Celsius.

source: https://leetcode.com/problems/convert-the-temperature/


== Suite de Fibonacci
Écrire un programme qui imprime les 100 premiers #link-fibb.

source: https://leetcode.com/problems/fibonacci-number/

== Fizz Buzz
Écrire un programme qui, pour les nombres entre 1 et 20 :

- si le nombre est divisible par 3 : imprime "Fizz"
- si le nombre est divisible par 5 : imprime "Buzz"
- si le nombre est divisible par 3 et par 5 : imprime "Fizzbuzz"
- sinon : on écrit le nombre

source: https://leetcode.com/problems/fizz-buzz/

== Escalier

Écrire un programme qui imprime un escalier de hauteur $n$.

L'escalier doit monter par la droite (comme ci-bas) et les marches
doit etre faite en `#`. 

Voici un example d'un escalier de hauteur 5.
```
    #
   ##
  ###
 ####
#####
```

== Palindrome
Écrire un programme qui vérifie si un mot est un #link-palindome.  On peut
aussi être intéressé à vérifier si un mot est un "presque palindrome",
c'est-à-dire un palindrome si l'on enlève tous les charactères qui ne sont 
pas des letters latines.

source: https://leetcode.com/problems/valid-palindrome/


== Text Dans Un Cadre

Écrire un programme qui encadre un texte dans un carré de `#`.


Exemple.

```
###############################################################################
#     Pariatur molestiae sint optio commodi incidunt et non. Velit vitae      #
###############################################################################
```



== La Numérotation des Titre de ce Document

Les titres de ce document sont en fait des sous-titres.  Le document est écrit en
#link-typst, mais s'il était écrit en html, les titres seraient des `<h2>`.  C'est plus joli
comme ça.

La numérotation des titres est automatique et de format `{titre}.{sous-titre}.{sous-sous-titre}`.
Il a donc fallu écrire une fonction qui transforme le format `{titre}.{sous-titre}.{sous-sous-titre}`
en  `{sous-titre}.{sous-sous-titre}`.  Par exemple, `0.2.a` devrait être transformé en `2.a`.


Écrivez un programme qui fait la même chose.

== didier au gym

didier aime bien l'aviron au gym.  La vitesse de l'aviron se compte en temps requis pour effectuer 500 meters.  Par exemple, 2 minute 44 secondes du 500 mètres.

Mardi dernier, didier a pris 22 minutes 5 seconds à faire ces premiers $5 000$ mètres.  Après 30 minutes, il a fait un total de 6811 mètres.

Écrire une programme qui:
- affiche la vitesse de didier pour les premier $5 000$ mètres.
- affiche la vitesse moyenne pour les 30 minutes.
- affice "Bravo!" si la vitesse moyenne pour les 30 minutes est plus élevé que la vitesse du premier $5 000$ mètres.


== Imprimer tous les dossiers du PATH
Pour obtenir une variable d'environment en C\# il faut utiliser la fonction 
`System.Environment.GetEnvironmentVariable(string)`.

Écrire une fonction qui imprime tous les dossiers sur le `PATH`.  Chaque dossier doit être imprimé sur une ligne différente.
