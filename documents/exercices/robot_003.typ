#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Robot Wanted League", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1)
}
#set heading(numbering: numbering-function, level: 1)


= Objectif

Votre mission, si vous l'accepter, est de créer un répertoire web de tous les 
vilains robots recherchés par la _Robot Wanted League™_.  Le président veut
ce site web au plus coupant et se garde le droit de mondifier ce qu'il veut
quand bon lui semble.



#rect(width: 100%, stroke: red)[
    #text(weight: "bold", size: 1.5em)[Évaluation] \
    Ce travail vaut 40% de la note final.  Le travail peut être fait en équipe
    de deux ou trois.  Vous pouvez partir de votre propre projet ou partir
    de celui que nous avons fait ensemble
    https://github.com/didiercrunch/RobotsWantedLeague.


    Les points dévaluations sont les suivants.

    1. Lisibilité du code.  Le code ne doit pas être élégant mais cohérent et 
       idiomatique au language C\#.
    2. Vous devez être capable d'explique ce que votre code fait.  Vous avez 
       le droit d'utiliser n'importe quel outil mais vous devez être capable
       de défendre vos décisions.
    3. Votre code doit implémenter les fonctionnalités décrites ci bas.  Il
       n'est pas nécessaire d'implémenter toutes les fonctionnalitées mais
       les fonctionnalitées doivent être bien implémentées.  Les solutions
       doivent être robustes.
    4. Le plus de points que vous implémentez, le mieux.


    Durant les cours qui nous reste, je vais me promener d'équipe en équipe pour
    vous aider et vous évaluer.  Mon objectif étant de m'assurer que vous compreniez
    ce que faites et que vous ne vous fiez pas trop à chat GPT.

    Le travail doit être remis, au plus tard le 29 septembre 2023 à midi.

]


== Produit Minimal Viable (fait en classe)

Il faut commencer par créer une page qui listera tous les robots recherchés
ainsi que, pour chaque robot recherché, une page avec les informations 
suivantes.

- Le nom du robot
- Le poid du robot
- La taille du robot
- Le pays ou le robot se trouve


Nous avons déjà trois robots à lister:

1. Alice, pèse 6 578kg pour 17.76m de haut.  La dernière fois qu'il a été vu il était en Albanie
2. Bob, pèse 10 871kg pour 15m de haut.  La dernière fois qu'il a été vu il était au Bénin
3. Cédric, pèse 1 002kg pour 3.12m de haut.  La dernière fois qu'il a été vu il était en Cambodge


== Ajouter Des Robots (fait en classe)

Finalement, les agents sur le terrain veulent être capable d'ajouter des robots
à l'application.  Les agents doivent être capable de specifier un nom de robot,
un poid, une taille et un pays pour créer un nouveau robot.



== Les Robots Ont La Bougeotte

Des agents nous ont dit que certain robots commence à changer de pays!  Vite, vite,
il faut continuer de les suivre à la trace.  Il faut absolument que les agents puissent
changer le lieu où se trouve un robot.

=== 
Je ne sais pas si c'est techniquement possible, mais il faudrait que ce soit possible
savoir où les robots on été dans le passé.  Ce serait stupide de perdre l'information
d'où était les robots.


== Trop d'Erreur

Beaucoup de nos agents proviennent du Cegep-Du-Vieux-Montréal et sont très limité
côté géographie.  Un agent à dit avoir vue le Robot Garry à Aghraba!  Aghraba c'est
le pays d'Aladin, ça n'existe pas!

== Recherche Par Pays

Bon, il y a maintenant beaucoup de robots un peu partout dans le monde.  J'ai
besoin d'une vue d'ensemble de tout ça.  J'aimerais être capable de rechercher 
les robots par pays et par continent.


== Liste D'Agents

Bon travail agents programmeurs!  Tout les agents aiment le site web et 
l'utilisent régulièrement.  Par contre, nous aimerions avoir une liste 
des agents qui travaille pour nous dans le site web.


Un agent a un nom, un numéro d'identification ainsi qu'un rayon d'action 
(continent).  Vous pouvez utiliser la base de donnée 
#link("https://good-hawk-21.deno.dev")[good-hawk-21] pour les images d'agents.

== Qui Fait Quoi?

Présentement, je suis tout perdu.  Je ne sais pas quel agent est affecté à quel
robot... Nous avons probablement plusieurs agents pour certain robots et zero
agent pour d'autre.. 

Pouvez-vous m'aider à organiser tout ça?

== Notes des Agents

Les agents veullent travailler de manière plus collaborative.  Ça serait formidable
si les agents pouvait ajouter des commentaires sur la page robot.  Et, pourquoi pas,
ajouter une note de dangerosité comme sur 
#link("https://www.rottentomatoes.com/")[rotten tomatoes].

== Agents Surchargé

Vous êtes capable de me dire quel agent est surchargé?  Si nous remarquons qu'une
régions est surchargé, nous allons devoir transférer des agents d'une région à 
une autre.
