#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Méthodes Statiques", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1)
}
#set heading(numbering: numbering-function, level: 1)


== Triangle 

Écrire	un	programme	qui	affiche	un	triangle	isocèle	 formé	d'étoiles.	La	hauteur	du triangle	 (c'est-à-dire	 son	 nombre	 de	 lignes)	 sera	 fourni	 en	 donnée.

Example:

```
string triangle = TriangleProblem.BuildTriangle(8);
Console.WriteLine(triangle);
```

Resultat:
```
     *
    ***
   *****
  *******
 *********
***********
```
== Carte D'Identification

Le but de cet exercices est de pratiquer les classes.  Pour
se faire, nous allons créer une classe représentant une carte
d'identité ayant comme attributs un nom, un numéro de téléphone
ainsi qu'un code postal.  Nous allons ensuite implémenter une 
méthode `IsValid()` sur le tout.

===
Créer une class `Name` qui représente un nom.

===
Créer une class `TelephonNumber` qui représente un 
numéro de téléphone (canadien).

===
Créer une class `PostalCode` représentant un code postal
(canadien).

===
Créer une méthode `IsValid` dans la classe `Name`.

===
Créer une méthode `IsValid` dans la classe `TelephonNumber`.

===
Créer une méthode `IsValid` dans la classe `PostalCode`.

===
Créer la class `IDCard` qui représente la carte 
d'identité du problème et implémenter la méthode `IsValid` 
en utilisant les composantes.


== Liste chaîné d'entier (difficile)
Implémenter une liste chaîné d'entier.


Une liste chaîné est une forme simple d'une liste en 
programmation.  Une liste chainé est formée par des
chaînons indépendants.  Chaque chaînon contient une 
valeur et un lien vers le prochain chaînon; sauf pour 
le dernier chaînon qui ne pointe vers rien.


#figure(
  image("../images/Singly-linked-list.png", width: 50%),
  caption: [
    Représentation d'une liste chaîné
  ],
)

La manière usuelle de créer une liste chaîner est de la 
créer par chaînon et de pratiquement oublier la liste 
elle même.  La méthode `Head()` d'un chaînon retourne la 
valeure de ce chaînon.  La méthode `Rest()` retourne le 
prochain chaînon.

Un example d'utilisation est illustré ci-bas.
```
IntChainLink lastLink = new IntChainLink(3);
IntChainLink secondLink = new IntChainLink(2, lastLink);
IntChainLink firstLink = new IntChainLink(1, secondLink);

Console.WriteLine(firstLink.Head()); // 1
Console.WriteLine(firstLink.Rest().Head()); // 2
Console.WriteLine(firstLink.Rest().Rest().Head()); // 3

```


*truc*

Vous allez probablement avoir besoin de l'opérateur null-forgiving `!` pour gérer les nulls.

https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/operators/null-forgiving


== Fractale Magique
Le but de cet exercice sera de générer un triangle sierpinski
de manière magique.

#figure(
  image("../images/Sierpinski_triangle.jpeg", width: 50%),
  caption: [
    Triangle de Sierpinski
  ],
)


Pour tracer un triangle de Sierpinski, il faudra suivre un point
choisi au hazard dans un triangle equilatéral de côté 10.  À 
chaque itération, le point bougera dans le triangle
et l'ensemble des lieux où passera le point créra
un triangle de Sierpinski.

Le point commence dans un lieu  quelconque à l'intérieur d'un
triangle.  Le point sautera d'un lieu à un autre en une série
d'étape. À chaque étape, le point sautera à mi-distance d'un
sommet choisi au hazard. C'est-à-dire, que pour choisir le lieu
où sautera le point, il faut choisir un sommet du triangle
au hazard; le milieu entre le point et le sommet sera la nouvelle
position du point.


=== 
Écrire une classe "Point" du plan catésien.

===
Faire en sorte que la méthode `ToString`de "Point"
resourne un beau JSON avec comme champs "x" et "y".

===
Écrire une méthode sur "Point" qui prend un autre point
et retourne le Point milieu.

===
Écrire une classe "Triangle" en utilisant la classe "Point"


===
Écrire un constructeur qui permet de créer un triangle
équilatéral avec un seul argument

===
Écrire une méthode à Triangle qui retourne un point
de sommet avec un index. C'est-à-dire, on associe à 
chaque sommet un nombre 0, 1 ou 2 et la méthode 
retournera le sommet en fonction du nombre.

=== 
Jouer avec la classe `Random` offerte par C\#.

- Essayer de generer un long aléatoire entre 0 et 10
- Essayer de generer un long aléatoire entre 0 et 100
- Essayer de generer 10 long aléatoire entre 0 et 10 de
  manière à ce que le programme retourne toujours les 
  même 10 nombre aléatoire, même après un redémarage.

https://learn.microsoft.com/en-us/dotnet/api/system.random?view=net-7.0


=== 
Ajouter à la classes Triangle une méthode qui prend une
instance de `Random` et retourne un sommet aléatoirement.

=== 
Écrire un fonction qui implémente l'algorithm et retourne
un string représentant un array JSON avec tous les lieux
où le point est passé.

```
public static string SierpinskiTriangle(int numberOfPoints){
    return """
    [{"x": 5, "y": 6}, 
     {"x": 2.5, "y": 8.0}, 
     {"x": 1.25, "y": 9.0}, 
     {"x": 0.625, "y": 4.5}]
    """;
}
```

===
Écrire une fonction qui prend en argument un string (qui sera 
l'array JSON décrit à la question précédente) et retoure la 
description vega-lite ci-dessous:

```
{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "data": {"values": <PLACER ICI LES VALEURES>},
  "width": 600,
  "height": 600,
  "mark": {"type": "point", "filled": true, "size": 2},
  "encoding": {
    "x": {"field": "x", "type": "quantitative"},
    "y": {"field": "y", "type": "quantitative"}
  }
}
```

===
Utiliser l'éditeur en ligne de véga-lite (https://vega.github.io/editor/#/edited) pour visualizer le triangle de Sierpinski.

=== (extra)
Est-ce que l'algorithme fonctionne avec un triangle non equilatéral?

=== (extra)
Est-ce que l'algorithme fonctionne avec un carré?

