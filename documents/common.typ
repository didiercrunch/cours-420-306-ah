#let params = toml("main.toml")

#let basic-footer(doc) = [
  #set page(
    paper: "us-letter",
    footer: [
      #set align(center)
      #set text(8pt)
      Page #counter(page).display(
        "1 de 1",
        both: true,
      ) #linebreak()
      #text(size: 7pt)[Dernière mise-à-jour: #params.at(default: "", "lastUpdated") | Identification: #params.at(default: "", "githash")]
    ]
  )
  #doc
]

#let horizontal-line = line(start: (16.666%, 0%), length: 66.66%, stroke: 0.5pt)

#let answer-line(paint: black, thickness: 0.5pt) = line(start: (0%, 1em), 
                                                       end: (100%, 1em), 
                                                       stroke: (paint: paint, thickness: thickness))

#let code-block-answer = [
    1. #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
]

#let answer-choices(choices) = {
   set list(marker: box(stroke: black, width: 0.66em, height: 0.66em))
   for choice in choices [
      - #choice
    ]
}

#let homework-hash-footer(result-hash) = [
    \
    \
    \
    #horizontal-line 
    #align(
      center,
      [
        #if result-hash != none [Le sha1 du devoir remis est #result-hash] else []
      ]
    )
]


#let basic-header(title, extra-header-line: none, result: none, result-hash: none, doc) = [
    #align(
      center,
      [
        #linebreak
        #image("images/logo-ahuntsic.png", width: 33%)
        Programmation Web côté serveur III\
        #text(weight: "bold", title)\
        #if extra-header-line != none [#extra-header-line] else []\
      ]
    )

    #if result != none [#align(right, result)] else []
    #horizontal-line 

    #doc

    #if result-hash != none [#homework-hash-footer(result-hash)] else []
]

#let comment(comment) = [
  #rect(width: 100%, stroke: red)[#text(red)[#comment]]
]
