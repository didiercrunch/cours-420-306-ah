#import "../common.typ": basic-footer, horizontal-line, basic-header

#let enums = link("https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/enum")[enums]


#let points-state = state("points", ());

#let points(n) = {
   points-state.update(lst => {lst.push(n); 
                             lst});
   points-state.display(lst => {
      let val = lst.last();
      if val < 2 [(#val punkto)] else [(#val punktoj)]
   })
}

#let total-points = locate(loc => points-state.at(
  query(<end>, loc)
    .first()
    .location()
).sum())

#show: basic-footer
#show: doc => basic-header(
   "Devoir 1 | Les Bases Du C#", 
   extra-header-line: [Ce devoir contient #total-points points],
   doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1) + "."
}
#set heading(numbering: numbering-function, level: 1)

= Instructions

- Ce premier devoir doit être remis pour le 11 septembre 2023 à minuit.
- Le  travail peut être fait en équipe de 1, 2 ou 3.
- Le travail doit être remis sous la forme d'une solution C\#, 
  compréssé en format zip, par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca
- Le travail doit contenir un petit README qui indique
  où sont les réponses aux questions.


== Moyenne d'une liste

=== #points(1)
Écrire une fonction qui prend une liste de doubles et qui calcule
la moyenne des éléments de cette liste.

=== #points(1)
Tester la fonction avec des tests unitaires.

== Ascii art

=== #points(3)
Écrire une fonction `CreateStaircase(int height, int width)` qui retourne
un `string` de 5 "marches" mesurant `height` de haut et `width`
de large.

Par example, ci-bas est le résultat de `CreateStaircase(2, 3)`.

```
            ###
            ###
         ######
         ######
      #########
      #########
   ############
   ############
###############
###############
```

=== #points(2)
Tester la fonction avec des tests unitaires.

== Tic-Tac-Toe

Le but de cet exercice est de créer une classe qui représente un jeu de
tic-tac-toe qui permettrait à deux joueurs de jouer.  Pour la simplicité,
le joueur *X* commence toujours.


=== #points(3)
Lire sur les #enums et créer un enum qui représente les joueurs du tic-tac-toe.

=== #points(2)
Lire sur les #enums et créer un enum qui représente l'état d'un "carré" du jeu de tic-tac-toe.

=== #points(4)
Créer une classe `TicTacToe` qui représente l'état du jeu.  Vous devez utiliser 
les enums créés plus haut.

=== #points(3)
Ajouter une méthode `XPlay` qui permet au joueur "X" de jouer.

=== #points(3)
Ajouter une méthode `OPlay` qui permet au joueur  "O" de jouer.

=== #points(2)
Ajouter une méthode `NextPlayer` qui retourne le prochain joueur à jouer.

=== #points(3)
Ajouter une méthode `TheWinnerIs` qui retourne le joueur gagnant.

=== #points(5)
Écrire un test unitaire qui donne un exemple de jeu complet.


== Approximation de la valeur de $pi$ par une méthode Monte-Carlo

Il est possible d'estimer la valeur de $pi$ par une méthode qui utilise
l'aléatoire. Pour se faire, il faut connaître la formule de l'aire 
d'un cercle $A = pi r^2$ où $A$ est l'aire du cercle et $r$ son rayon.

L'idée principale de la méthode est d'estimer l'aire d'un cercle  de rayon
prédéfini.  En effet, avec l'aire et le rayon connus,
nous avons que $pi := A / r^2$.  Dans ce cas-ci, prendre un cercle de
rayon $1$ est très judicieux car la formule ce simplifira par $pi := A$.


Donc, comment faire pour estimer l'aire d'un cercle?  La méthode que nous
allons utiliser nécessite d'inscrire le cercle dans un carré comme illustré
ci-bas.

#figure(
  square(size: 40pt, inset: 0pt)[#circle(radius: 20pt)],
  caption: [Cercle inscrit dans un carré],
) 

Comme il est facile de connaître l'aire du carré $A_c$, nous allons essayer 
de trouver la proprotion $p$ du carré qui est couverte par le cercle.  Pour
se faire, nous allons lancer des points au hazard dans le carré; la 
proprotion $p$ du carré qui est couverte par le cercle sera alors 
estimée par le nombre de point ayant tombé dans le cercle divisé par le 
nombre total de point.

=== #points(1)
Copier/Utiliser la classe `Point` vue en classe.

=== #points(2)

Créer une classe `Cercle` centrée en $(0, 0)$ et de rayon $r$.

=== #points(2)
Ajouter une méthode `IsIn` à `Cercle` qui prend en argument un `Point` et retoure `true`/`false`
dépendamment si le point est à l'intérieur ou l'extérieur du cercle.


=== #points(2)
Créer une classe `Square` centrée en $(0, 0)$, de coté $c$.

=== #points(1)
Créer une classe `Area` du carré qui retourne l'aire du carré.

=== #points(2)
Créer une méthode `RandomPoint` dans `Square` qui prend en argument
un object de classe Random et retourne un point aléatoire (uniformément 
distribué) situé dans le carré.

=== #points(5)

Créer un méthode statique `PiEstimate` qui prend un entier $n$ en paramêtre et
retourne une estimation de $pi$ faite avec $n$ points.

=== #points(3)
Écrire un test unitaire qui démontre que votre valeur de $pi$ n'est pas 
trop loin de la valeur usuelle $3.1415926535897932384626433832795028841971$.

<end>




