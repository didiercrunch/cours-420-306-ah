#import "../common.typ": basic-footer, horizontal-line, basic-header, 
#import "../common.typ": answer-line, code-block-answer


// #let answer-line() = none
// #let code-block-answer = none

#let points-state = state("points", ());

#let points(n) = {
   points-state.update(lst => {lst.push(n); 
                             lst});
   points-state.display(lst => {
      let val = lst.last();
      if val < 2 [(#val punkto)] else [(#val punktoj)]
   })
}

#let total-points = locate(loc => points-state.at(
  query(<end>, loc)
    .first()
    .location()
).sum())


#let extra-header = [
    Cet examen contient #total-points points 

    Nom: #overline(offset: 0em, [`                                        `]) \
    Numéro d'étudiant: #overline(offset: 0em, [`                            `])
]

#show: basic-footer
#show: doc => basic-header(
   "Examen Final", 
   extra-header-line: extra-header, 
   doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1) + "."
}
#set heading(numbering: numbering-function, level: 1)

= Instructions

- Vous avez le droit à une feuille de note, manuscrite, recto-verso.
- Vous avez 2 heures pour faire l'examen.

== Points Historiques

=== #points(1)
De quel langage le C\# est-il inspiré?  Nous disons souvent que le 
C\# est un "XXXX-like programming language". 

#answer-line()
#answer-line()


=== #points(1)
Quel langage ressemble beaucoup au C\# et est son principal compétiteur?

#answer-line()
#answer-line()


== Écriture de Petits Programmes

=== Les accolades #points(1)

Lequel des deux morceaux de code suivant est le plus idiomatique pour le C\# ?

*A )*
```csharp
for(int i = 0; i < 10; i ++){
   Console.WriteLine("The value of i in " + i);
}
```

*B )*
```csharp
for(int i = 0; i < 10; i ++)
{
   Console.WriteLine("The value of i in " + i);
}
```

#answer-line()


=== #points(4)
Écrire une fonction qui prend une liste de doubles et qui calcule
la moyenne des éléments de cette liste.

#code-block-answer

=== #points(1)
Écrire le programme HelloWorld.

#code-block-answer

=== #points(4)

Écrire une classe qui représente une canette 
dans une machine distributrice.  Une canette
possède un attribut format et contenue.

#code-block-answer

=== #points(1)

Écrire un test pour la fonction `PrixIphone` telle
que décrite dans le code ci-bas.  Écrivez juste
le corps de la méthode test.

```csharp
class Util {
   public static int PrixIphone(string version) {
      if (version == "15 pro") {
         return 1449.99;
      }
      if (version == "15 pro Max") {
         return 1749.99;
      }
      return -1;
   }
}
```

#code-block-answer

== Lecture de code #points(3)

Écrire une fonction qui prend une variable `nom` de type
string nullable et qui retourne le nom si le nom n'est pas 
nulle et `"inconnu"` si le nom est nul.

#code-block-answer

=== #points(3)
Dessiner le ascii art fait par la fonction ci-bas.

```csharp
static void WriteDiagonal(int n){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < i - 1; j++){
            Console.Write(" ");
        }
        Console.WriteLine("#");
    }
}
```

#code-block-answer

=== #points(3)

Décrire ce que la fonction ci-bas fait.  Comment la nommeriez-vous?

```csharp
static int XXXXXX(string time){
   string[] parts = time.Split(":");
   if (parts.Length != 2){
      throw new Exception("input must be in the form 00:00");
   }
   return 60 * Int32.Parse(parts[0]) + Int32.Parse(parts[1]);
}
```

#code-block-answer


=== #points(1)
Quel est le fameux nom de la fonction suivante?

```csharp
static int XXXX(int n){
   if (n < 0){
      return 1;
   }
   return XXXX(n -1 ) + XXXX(n - 2);
}
```

#answer-line()
#answer-line()

=== #points(3)
Que fait la fonction suivante?

```csharp
static Dictionary<string, int> XXXXX(List<string> list) {
    Dictionary<string, int> ret = new Dictionary<string, int>();
    foreach (var item in list) {
        if (!ret.ContainsKey(item)){
            ret[item] = 1;
        } else {
            ret[item] += 1;
        }
    }

    return ret;
}

```

#answer-line()
#answer-line()
#answer-line()

=== #points(2)

Donner un bon nom pour la méthode "QUELQUECHOSE" ci-bas.

```csharp
class Rectangle
{
    private readonly double height;
    private readonly double width;

    public Rectangle(double height, double width){
        this.height = height;
        this.width = width;
    }

    public double QUELQUECHOSE(){
        return width + height + width + height;
    }
}
```

#answer-line()
#answer-line()

== Questions théoriques

=== #points(2)
Quelle est la différence entre une méthode statique et 
une méthode non statique.

#answer-line()
#answer-line()

=== #points(2)
Comment distingue-t-on le constructeur d'une classe?

#answer-line()
#answer-line()
#answer-line()

=== #points(2)
Comment doit-on nommer un namespace?

#answer-line()
#answer-line()

=== #points(2)
Quelle est la différence entre un `float` et  un `double`?

#answer-line()
#answer-line()
#answer-line()

=== #points(3)
Expliquer les trois partie d'une boucle for:
```csharp
for(PARTIE_1; PARTIE_2; PARTIE_3){
   
}
```

#answer-line()
#answer-line()
#answer-line()
#answer-line()
#answer-line()
#answer-line()

=== Commentaires #points(1)
Donnez un exemple de commentaires en C\#.

#answer-line()
#answer-line()
#answer-line()


== ASP.net

=== #points(3)
Quelle méthode, de quelle classe, est utilisée lorsque l'URL `/robots/listrobots` est frappée?

#answer-line()
#answer-line()
#answer-line()

=== #points(3)
Quel est la signature de la méthode qui est frappée par l'URL `/robots/getrobot?name=El_Chapo`?

#answer-line()
#answer-line()
#answer-line()

=== #points(4)

À quoi ressemble le résultat de la vue razor suivant?

```csharp
@{
    List<string> countries = new List<string> {"Canada", "USA", "Mexico" };
}
<ul class="list-group">
  @foreach (string country in countries) {
    <li class="list-group-item">
        @country
    </li>
  }
</ul>
```

#code-block-answer

<end>




