#import "../../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Mini Examen 1", doc)



#let question-1 = [
    Quel est le nom du programme utilisé pour controller un projet C\# ?
    #answer-line()
]

#let question-2 = [
    Comment se nomme le projet principal d'un projet C\# ?
    #answer-line()
]

#let question-3 = [
    Quel est l'extension des fichiers C\# ?
    #answer-line()
]

#let question-4 = [
    Comment se nomme les "petits projets" où se trouvent le code dans un projet C\# ?
    #answer-line()
]

#let question-5 = [
    Comment un terminal (shell) fait pour trouver le programme à executer?  Par exemple,
    comment votre terminal sait où trouver le programme `cat` dans la commande
    `cat text.txt`.
    #answer-line()
    #answer-line()
    #answer-line()
    #answer-line()
    #answer-line()
]


#enum(
    tight: false,
    enum.item(question-1),
    enum.item(question-2),
    enum.item(question-3),
    enum.item(question-4),
    enum.item(question-5),
)