#import "../../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Pré Examen", doc)

#let home_os = [
    Quel système d'exploitation utilisez-vous pour développer habituellement?
    #answer-choices(("Linux", "macOS", "Microsoft Windows"))
]

#let github = [
    Possédez-vous un compte github ou gitlab?  Êtes-vous à l'aise de l'utiliser dans
    le cadre de ce cours?
    #answer-line()
]

#let ide = [
    Quel Environnement de développement (IDE) utilisez-vous d'habitude?
    #answer-line()
]


#let db = [
    Avez-vous déjà utilisé une base de données? Si oui, laquelle?
    #answer-line()
]

#let webserver = [
    Avez-vous déjà implémenté une application web? Si oui, vous utilisiez quelles technologies?
    #answer-line()
]


#let what_is_ls = [
    Que fait la commande `ls` fait sous Linux?
    #answer-line()
]

#let programming_languages = [
    Quels languages de programmation connaissez-vous?
    #answer-line()
]


#let why_favourite_language = [
    Quel est votre language de programmation préféré et pourquoi?\
    #answer-line()
    #answer-line()
]

#let average_function = [
    Pouvez-vous  écrire une fonction qui calcule la moyenne d'une liste de nombres dans ce language?
    #code-block-answer
]

#let simple_arythimethic_question = [
    Que vaut $2 + 3 times 5$ ? 
    #answer-line()
]

#let float_and_int = [
    C'est quoi la différence entre un "float" et un "int"?
    #answer-line()
]

#let english = [
    Are you able to maintain a conversation in English?  Do you speak another language than English or French?
]

#enum(
    tight: false,
    enum.item(home_os),
    enum.item(github),
    enum.item(ide),
    enum.item(db),
    enum.item(webserver),
    enum.item(programming_languages),
    enum.item(float_and_int),
    enum.item(what_is_ls),
    enum.item(simple_arythimethic_question),
    enum.item(english),
    enum.item(why_favourite_language),
    enum.item(average_function),
)